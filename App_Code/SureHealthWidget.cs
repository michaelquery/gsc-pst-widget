using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Web;

namespace GSC
{
	public class SureHealthWidget : System.Web.UI.Page
	{
		protected static string[] getAllowedDomains()
		{
			// Load from file
			var path = HttpContext.Current.Server.MapPath("~/App_Data/allowed-domains.txt");
			return File.ReadAllLines(path)
				.Select(line => line.Trim().ToLower())
				.Where(line => line.Length > 0 && !line.StartsWith("#"))
				.ToArray();
		}

		/**
		 * Tests if the referrer domain is in the whitelist.
		 * If so, returns the whitelisted domain for the x-frame-origin header,
		 * otherwise returns null.
		 */
		public string Authorize()
		{
			var referrer = Request.UrlReferrer;
			if (referrer == null)
			{
				return null;
			}
			var refUrl = referrer.ToString().ToLower();
			return Array.Find(getAllowedDomains(), url => refUrl.StartsWith(url));
		}
	}
}
