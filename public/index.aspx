<%@ Page Language="C#" Debug="true" Inherits="GSC.SureHealthWidget" %>
<%
    var domain = Authorize();
    if (domain != null)
    {
        Response.Headers.Add("X-Frame-Options", "allow-from " + domain);
    }
    else
    {
        //Response.Headers.Add("X-Frame-Options", "sameorigin");
        Response.StatusCode = 303;
        Response.Headers.Add("Location", "/");
        Response.End();
    }
    var referrer = Request.UrlReferrer;
    var urlRef = referrer != null ? referrer.ToString() : "";
%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <meta name="viewport" content="initial-scale=1, width=device-width"/>
        <link rel="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="css/app.css"/>
        <script src="js/promise-polyfill.min.js" defer></script>
        <script src="js/fetch-polyfill.umd.js" defer></script>
        <script src="js/jquery-1.12.4.min.js" defer></script>
        <script src="js/jquery-ui.min.js" defer></script>
        <script src="js/app.js" defer></script>
        <title>SureHealth Product Selector Tool</title>
    </head>
    <body>
        <!-- Referrer URL: '<%=urlRef%>' -->
        <header>
            <div>
                <img src="images/headerlogos.png" alt="Sure Health - GSC Logo">
            </div>
        </header>
        <div class="wrapper">
            <div id="loader" class="animated hidden">
                <h1 tabindex="-1">We're on it.</h1>
                <img src="images/loading_anim.gif" alt="Loading..." tabindex="-1">
            </div>
            <div class="intro" id="intro">
                <h1 class="line-under">Need Individual Health and Dental Coverage?</h1>
                <h2>
                    Let's find a SureHealth plan for you.
                </h2>
                <button style='cursor: pointer' id="get-quote-btn"><img src="images/button-chevron.svg" alt=""><img src="images/button-chevron-green.svg" alt="">Get a Quote</button>
            </div>
            <div id="the-form" class="animated hidden">
                <form action="">
                    <div id="qset1">
                        <h1 class="line-under " tabindex="-1">Three quick questions</h1>
                        <label for="coveragetype" class="no-padding-top">I need coverage for</label>
                        <div class="select-wrapper">
                            <select name="coveragetype" id="coveragetype">
                                <option disabled selected value></option>
                                <option value="0">Myself</option>
                                <option value="1">Myself & my child(ren)</option>
                                <option value="2">Myself, my spouse/partner & our child(ren)</option>
                                <option value="3">Myself & my significant other</option>
                            </select>
                            <div class="select-val">
                                <span></span>
                            </div>
                        </div>
                        <label for="coveragetype">I live in</label>
                        <div class="select-wrapper">
                            <select name="province" id="prov">
                                <option disabled selected value></option>
                                <option value="0">Alberta</option>
                                <option value="1">British Columbia</option>
                                <option value="2">Manitoba</option>
                                <option value="3">New Brunswick</option>
                                <option value="4">Newfoundland & Labrador</option>
                                <option value="5">Northwest Territories</option>
                                <option value="6">Nova Scotia</option>
                                <option value="7">Nunavut</option>
                                <option value="8">Ontario</option>
                                <option value="9">Prince Edward Island</option>
                                <option value="10">Quebec</option>
                                <option value="11">Saskatchewan</option>
                                <option value="12">Yukon</option>
                            </select>
                            <div class="select-val">
                                <span></span>
                            </div>
                        </div>
                        <label for="age">I'm</label>
                        <div class="select-wrapper">
                            <select name="age" id="age">
                                <option disabled selected value></option>
                                <option value="18">18 years old</option>
                                <option value="19">19 years old</option>
                                <option value="20">20 years old</option>
                                <option value="21">21 years old</option>
                                <option value="22">22 years old</option>
                                <option value="23">23 years old</option>
                                <option value="24">24 years old</option>
                                <option value="25">25 years old</option>
                                <option value="26">26 years old</option>
                                <option value="27">27 years old</option>
                                <option value="28">28 years old</option>
                                <option value="29">29 years old</option>
                                <option value="30">30 years old</option>
                                <option value="31">31 years old</option>
                                <option value="33">33 years old</option>
                                <option value="33">33 years old</option>
                                <option value="34">34 years old</option>
                                <option value="35">35 years old</option>
                                <option value="36">36 years old</option>
                                <option value="37">37 years old</option>
                                <option value="38">38 years old</option>
                                <option value="39">39 years old</option>
                                <option value="40">40 years old</option>
                                <option value="41">41 years old</option>
                                <option value="44">44 years old</option>
                                <option value="44">44 years old</option>
                                <option value="44">44 years old</option>
                                <option value="45">45 years old</option>
                                <option value="46">46 years old</option>
                                <option value="47">47 years old</option>
                                <option value="48">48 years old</option>
                                <option value="49">49 years old</option>
                                <option value="50">50 years old</option>
                                <option value="51">51 years old</option>
                                <option value="52">52 years old</option>
                                <option value="53">53 years old</option>
                                <option value="54">54 years old</option>
                                <option value="55">55 years old</option>
                                <option value="56">56 years old</option>
                                <option value="57">57 years old</option>
                                <option value="58">58 years old</option>
                                <option value="59">59 years old</option>
                                <option value="60">60 years old</option>
                                <option value="61">61 years old</option>
                                <option value="62">62 years old</option>
                                <option value="63">63 years old</option>
                                <option value="64">64 years old</option>
                                <option value="65">65 years old</option>
                                <option value="66">66 years old</option>
                                <option value="67">67 years old</option>
                                <option value="68">68 years old</option>
                                <option value="69">69 years old</option>
                                <option value="70">70 years old</option>
                                <option value="71">71 years old</option>
                                <option value="72">72 years old</option>
                                <option value="73">73 years old</option>
                                <option value="74">74 years old</option>
                            </select>
                            <div class="select-val">
                                <span></span>
                            </div>
                        </div>
                        <button style='cursor: pointer' id="find-plan-btn" class="padding-top">
                            <img src="images/button-chevron.svg" alt=""><img src="images/button-chevron-green.svg" alt="">Let's Find a Plan!
                        </button>
                    </div>
                    <div id="qset2" class="animated hidden">
                        <h1 class="line-under" tabindex="-1">Bonus Question!</h1>
                        <h2>We noticed that you have dependants.</h2>
                        <label id="deps-label1" for="deps">I have</label>
                        <div class="select-wrapper">
                            <select name="deps" id="deps" aria-labelledby="deps-label1 deps-label2">
                                <option disabled selected value></option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                            </select>
                            <div class="select-val">
                                <span></span>
                            </div>
                        </div>
                        <p class="label" id="deps-label2">
                            Dependents under the age 21
                        </p>
                        <button style='cursor: pointer' id="find-plan-btn-2" class="padding-top">
                            <img src="images/button-chevron.svg" alt=""><img src="images/button-chevron-green.svg" alt="">Let's Find a Plan!
                        </button>
                    </div>

                </form>
            </div>
            <div id="results" class="animated hidden">
                <h1 class="line-under" tabindex="-1">We recommend</h1>
                <div class="plan-block">
                    <h2 id="plan-name"></h2>
                    <h3>Guaranteed Acceptance</h3>
                    <p id="price">$<span id="price-amount"></span><sup>.00</sup>
                        <span class="per-month">per month</span>
                    </p>
                    <button style='cursor: pointer' id="whats-covered-btn"><img src="images/button-chevron.svg" alt=""><img src="images/button-chevron-green.svg" alt="">See What's Covered</button>
                </div>
            </div>
            <div id="error-display" class="animated hidden">
                <h1 class="line-under" tabindex="-1">We're sorry</h1>
                <div class="plan-block">
                    <h2>There was a problem with your request</h2>
                </div>
            </div>
        </div>
        <footer>
            <span id="btn-why" class="hidden">Why are we asking these questions?<br/>We just want to make sure your quote is accurate.</span>
            <a href="https://www.surehealth.ca/" id="btn-browse" class="animated" target="_blank">Browse on SureHealth.ca</a>
        </footer>
    </body>

</html>
