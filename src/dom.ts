// DOM utils

/** Shorthand for getElementById */
export function $e<T extends Element = HTMLElement>(id: string): T {
  const el = document.getElementById(id);
  if (el == null) {
    throw new Error(`Could not find element with id '${id}' in dom`);
  }
  return el as unknown as T;
}

/** Shorthand for querySelector */
export function $q<T extends Element = HTMLElement>(s: string): T {
  const el = document.querySelector(s) as unknown as T;
  if (el == null) {
    throw new Error(`Could not find element matching selector '${s}' in dom`);
  }
  return el as unknown as T;
}
