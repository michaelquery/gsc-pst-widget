import 'ie-array-find-polyfill';
import {$e, $q} from './dom';

const errorDisplay = $e('error-display');
const priceamount = $e('price-amount');
const planname = $e('plan-name');
const loader = $e('loader');
const getQuoteBtn = $e('get-quote-btn');
const findPlanBtn = $e('find-plan-btn');
const findPlanBtn2 = $e('find-plan-btn-2');
const whatsCoveredBtn = $e('whats-covered-btn');
const btnWhy = $e('btn-why');
const btnBrowse = $e('btn-browse');
const introDiv = $e("intro");
const mainForm = $e("the-form");
const qset1 = $e("qset1");
const qset2 = $e("qset2");
const resultDisplay = $e("results");
const qset1H1 = $q("#qset1 h1");
const qset2H1 = $q("#qset2 h1");
const resultsH1 = $q("#results h1");
const loaderImg = $q("#loader img");
const errorH1 = $q("#error-display h1");
const qURL = window.location.hostname.indexOf("surehealth") == -1 ? "https://api-uat.gs-newtech.ca/healthassistservices1/api/quoteset/" : "https://api.gscservices.ca/healthassistservices/api/quoteset/"
const statusURL = (window.location.hostname.indexOf("surehealth") == -1 && window.location.hostname.indexOf("gs-newtech") == -1) ? "http://cn.pixelpusher.ca/status.aspx" : "/status.aspx";
const TID = "UA-70682781-1";

const STATE_SCREEN_1 = "SCREEN1";
const STATE_SCREEN_2 = "SCREEN2";
const STATE_SCREEN_3 = "SCREEN3";
const STATE_SCREEN_4 = "SCREEN4";

let state = STATE_SCREEN_1;
let accessToken: string | null = null;

let zfPlan: any;

let cTypeValue: string;
let provValue: string;
let ageValue: string;
let depsValue: string;

const CID: any = generateGaClientId() || getUrlParameter("cid") || "111";
// const HOST: any = getUrlParameter("host") || "test.com";
const HOST: any = new URL(document.referrer).hostname || getUrlParameter('host') || "(not set)";

const AFFILIATE_ID: any = getUrlParameter('affiliate') || "(not set)";

let quoteData: any;
let userData: any;

function generateGaClientId(): string {

  const ts = Math.round(+new Date() / 1000.0);
  let rand;

  try{
    const uu32 = new Uint32Array(1);
    rand = crypto.getRandomValues(uu32)[0];
  } catch(e) {
    rand = Math.round(Math.random() * 2147483647);
  }

  return [rand, ts].join('.');

}

function isZF (plan: any) {
  return plan.PlanName === 'ZONE FUNDAMENTAL PLAN';
}
function resetForm() {
  cTypeValue = "";
  provValue = "";
  ageValue = "";
  depsValue = "";
  $e<HTMLSelectElement>('coveragetype').value = "";
  $e<HTMLSelectElement>('prov').value = "";
  $e<HTMLSelectElement>('age').value = "";
  $e<HTMLSelectElement>('deps').value = "";
  state = STATE_SCREEN_1;
  qset1.classList.remove("hidden");
  qset2.classList.add("hidden");
  mainForm.classList.add("hidden");
  introDiv.classList.remove("hidden");
  resultDisplay.classList.add("hidden");
  findPlanBtn.classList.remove("hidden");
  $(".select-val > span").text("");
}

whatsCoveredBtn.addEventListener('click', whatsCoveredBtnClick);
whatsCoveredBtn.addEventListener('touchend', whatsCoveredBtnClick);

function whatsCoveredBtnClick() {
  if (parseInt(ageValue, 10) < 55) {
    window.open("http://www.surehealth.ca/#!/recommended-plans/4,6,7/3", '_blank');
  } else {
    window.open("http://www.surehealth.ca/#!/recommended-plans/4,6,11/3", '_blank');
  }
  resetForm();
  sendGA({
    v: "1",
    tid: TID,
    cid: CID,

    t: "event",
    ec: "SureHealth Widget - Engagement",
    ea: "Form Completion",
    el: "Form Completion - Step 3",
    ev: "300",

    dh: HOST,
    dp: "/widget/surehealth",
    dt: "SureHealth Widget - " + HOST,
    cd1: '(not set)',
    cd3: HOST,
    cd4: AFFILIATE_ID
  });
}

getQuoteBtn.addEventListener('click', getQuoteBtnClick);
getQuoteBtn.addEventListener('touchend', getQuoteBtnClick);

function getQuoteBtnClick() {
  introDiv.classList.add("hidden");
  mainForm.classList.remove("hidden");

  btnWhy.classList.remove("hidden");
  btnBrowse.classList.add("hidden");

  state = STATE_SCREEN_2;
  qset1H1.focus();
  sendGA({
    v: "1",
    tid: TID,
    cid: CID,

    t: "event",
    ec: "SureHealth Widget - Engagement",
    ea: "Form Completion",
    el: "Form Completion - Step 1",
    // ev: "300",

    dh: HOST,
    dp: "/widget/surehealth",
    dt: "SureHealth Widget - " + HOST,
    cd1: '(not set)',
    cd3: HOST,
    cd4: AFFILIATE_ID
  });
}

findPlanBtn.addEventListener('click', findPlanBtnClick);
findPlanBtn.addEventListener('touchend', findPlanBtnClick);

findPlanBtn2.addEventListener('click', findPlanBtnClick2);
findPlanBtn2.addEventListener('touchend', findPlanBtnClick2);

function findPlanBtnClick (e: Event) {
  cTypeValue = $e<HTMLSelectElement>('coveragetype').value;
  provValue = $e<HTMLSelectElement>('prov').value;
  ageValue = $e<HTMLSelectElement>('age').value;

  if (state == STATE_SCREEN_2 && (cTypeValue == "" || provValue == "" || ageValue == "")
  || state == STATE_SCREEN_3) {
    e.preventDefault();
    return;

  }
  sendGA({
    v: "1",
    tid: TID,
    cid: CID,

    t: "event",
    ec: "SureHealth Widget - Engagement",
    ea: "Form Completion",
    el: "Form Completion - Step 2",
    ev: "300",

    dh: HOST,
    dp: "/widget/surehealth",
    dt: "SureHealth Widget - " + HOST,
    cd1: '(not set)',
    cd3: HOST,
    cd4: AFFILIATE_ID
  });

  if ((cTypeValue === "1" || cTypeValue === "2")) {
    qset1.classList.add("hidden");
    qset2.classList.remove("hidden");

    qset2H1.focus();
    state = STATE_SCREEN_3;

  } else {
    qset1.classList.add("hidden");
    qset2.classList.add("hidden");
    getQS();
  }
  // console.log(a_token_data);
  e.preventDefault();
}
function findPlanBtnClick2 (e: Event) {
  depsValue = $e<HTMLSelectElement>('deps').value;

  if ((depsValue == "")) {
    e.preventDefault();
    return;

  }

  sendGA({
    v: "1",
    tid: TID,
    cid: CID,

    t: "event",
    ec: "SureHealth Widget - Engagement",
    ea: "Form Completion",
    el: "Form Completion - Step 2b",
    ev: "300",

    dh: HOST,
    dp: "/widget/surehealth",
    dt: "SureHealth Widget - " + HOST,
    cd1: '(not set)',
    cd3: HOST,
    cd4: AFFILIATE_ID
  });
    qset1.classList.add("hidden");
    qset2.classList.add("hidden");
    // findPlanBtn.classList.add("hidden");


    getQS();
    e.preventDefault();
}

async function getToken() {
  if (accessToken) {
    return accessToken;
  }
  // const result = await fetch('http://cn.pixelpusher.ca/status.aspx');
  const result = await fetch(statusURL);
  accessToken = result.headers.get("access_token");
  return accessToken;
}

function serialize (obj: any) {
  const str: Array<any> = [];
  for (let p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
    }
  return str.join("&");
}

function getUrlParameter (sParam: string) {
  const sPageURL = decodeURIComponent(window.location.search.substring(1));
  const sURLVariables = sPageURL.split('&');
  for (let i = 0; i < sURLVariables.length; i++) {
    const sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ? true : sParameterName[1];
    }
  }
}

function sendGA( gaData: object) {
  const result = fetch('https://www.google-analytics.com/collect?'+serialize(gaData), {});
}

sendGA({
  v: "1",
  tid: TID,
  cid: CID,

  t: "event",
  ec: "SureHealth Widget - Engagement",
  ea: "Form Completion",
  el: "Form Completion - Step 0",
  ev: "300",

  dh: HOST,
  dp: "/widget/surehealth",
  dt: "SureHealth Widget - " + HOST,
  cd1: '(not set)',
  cd3: HOST,
  cd4: AFFILIATE_ID
});

async function getQS() {
  loader.classList.remove("hidden");
  loaderImg.focus();
  btnWhy.classList.add("hidden");
  const token = await getToken();
  userData = {
    Age: ageValue,
    CoverageType: cTypeValue,
    Dependents: depsValue,
    Province: provValue
  };
  const result = await fetch(qURL, {
    method: "POST",
    headers: {
      Authorization: 'Bearer ' + token,
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(userData)
  });

  const data = await result.json();

  loader.classList.add("hidden");
  if (data.ErrorDescription || data.Message) {
    errorDisplay.classList.remove("hidden");
    errorH1.focus();
  } else {
    quoteData = data;
    resultDisplay.classList.remove("hidden");
    state = STATE_SCREEN_4;

    zfPlan = quoteData.PlanQuotes.find(isZF);
    planname.innerHTML = zfPlan.PlanName;
    priceamount.innerHTML = zfPlan.Premium;
    const d = {
      qData: quoteData,
      uData: userData
    };
    sessionStorage.setItem("sessionUserData", JSON.stringify(d));

    btnBrowse.classList.remove("hidden");

    resultsH1.focus();
  }
}

// $( "select" ).selectmenu();
// $( "#age" ).selectmenu().addClass( "overflow" );

$("select").css("opacity", 0);

$("select").each(function() {
  $(this).parent().find(".select-val > span").text($(this).find(":selected").text());
});

$("select").change(function() {
  $(this).parent().find(".select-val > span").text($(this).find(":selected").text());
  $(this).css("opacity", 0);
});

// $( "select" ).click(function(){
//     $(this).css("opacity", 0);

///////////////////////////////////////////////////////////
// For browserify-hmr
// See browserify-hmr module.hot API docs for hooks docs.
declare const module: any; // tslint:disable-line no-reserved-keywords
if (module.hot) {
  module.hot.accept();
}
///////////////////////////////////////////////////////////
